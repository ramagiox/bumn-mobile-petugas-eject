import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { Container, Header, Content, Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body } from 'native-base';
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';
export default class ThankYou extends Component {
  render() {
    return (
      <Container>
        <Header />
        <Content>
          <Card style={{flex: 0}}>
            <CardItem>
              <Body>
              <Text style={{fontSize: 40, flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: "10%",marginRight:"10%",marginLeft:"10%",width:responsiveWidth(80) }}>
                Thank You
                </Text>
              </Body>
            </CardItem>
            <CardItem>

                  <Text style={{flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: "10%",marginRight:"10%",marginLeft:"10%",width:responsiveWidth(80), height:responsiveWidth(80)  }}>Laporan Anda akan diteruskan ke Command Center Kementrian BUMN</Text>
                
            </CardItem>
          </Card>
          <Button primary style={{flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: "10%",marginRight:"10%",marginLeft:"10%",width:responsiveWidth(80) }}
                                onPress={() => this.props.navigation.navigate("Beranda")}
                                >
                                    <Text style={{}}>OK</Text>
                                </Button>
        </Content>
      </Container>
    );
  }
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: 'green',
    },
    bigText: {
      fontSize: 40,
      color: 'black',
      textAlign:"center"
    },
    
  });
